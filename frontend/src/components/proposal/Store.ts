import { writable } from 'svelte/store';
import type { Asset } from '../portfolio/Asset';

export const initial: Asset[] = [];

const createProposals = (initPP: Asset[]) => {
  const {subscribe, set} = writable(initial);
  return {
    set,
    subscribe,
    reset: () => set(initial)
  }
}

export const ProposalStore = createProposals(initial);