import { writable } from 'svelte/store';
import type { Asset } from './Asset';


// TODO: is there a better way to initialize?
const initial: Asset[] = []

function createPortfolio(initPF: Asset[]) {

  const {subscribe, set, update} = writable(initPF);

  return {
    subscribe,
    add: (a: Asset) => update(pf => {
      let found = false;
      pf.forEach(p => {
        if (p.id === a.id) {
          found = true;
        }
      })
      if (!found) {
        pf.push(a)
      }
      return pf
    }),
    setAssetNumber: (a: Asset, n: number) => update(pf => {
      pf.forEach(p => {
        if (p.id === a.id) {
          p.number = n;
        }
      })
      return pf
    }),
    delete: (a: Asset): void => update(pf => {
      return pf.filter(p => p.id !== a.id)
    }),
    reset: () => set(initial)
  };
}

export const Portfolio = createPortfolio(initial);