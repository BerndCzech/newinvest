export interface Asset {
  // uuid
  id: string | bigint;
  name: string;
  // number of shares hold
  number?: number;
  price?: number;
  // price * number
  volume?: number;
}