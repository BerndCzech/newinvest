export interface Measure {
  measure: string;
  from: number;
  to: number;
  inverse?: boolean;
}

export interface Plot {
  time: string[] | number[];
  before: number[];
  after: number[];
}