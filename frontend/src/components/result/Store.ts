import { writable } from 'svelte/store';
import type { Measure, Plot } from "./Summary"

export const initialMeasure: Measure[] = [];

const createMeas = (initMeas: Measure[]) => {
  const {subscribe, set} = writable(initMeas);
  return {
    set,
    subscribe,
    reset: () => set(initialMeasure)
  }
}
export const MeasStore = createMeas(initialMeasure);

export const initialPlot: Plot = {} as Plot;

const createPlot = (initPlt: Plot) => {
  const {subscribe, set} = writable(initPlt);
  return {
    set,
    subscribe,
    reset: () => set(initialPlot)
  }
}

export const PlotStore = createPlot(initialPlot);