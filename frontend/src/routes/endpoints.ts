export const propose = import.meta.env.DEV ? `http://localhost:4000/propose` : '/propose';
export const summary = import.meta.env.DEV ? `http://localhost:4000/summary` : '/summary';
export const plot = import.meta.env.DEV ? `http://localhost:4000/plot` : '/plot';
export const search = import.meta.env.DEV ? `http://localhost:4000/search` : '/search';
