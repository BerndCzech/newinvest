<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

[![Contributors][contributors-shield]][contributors-url]
[![MIT License][license-shield]][license-url]

<!-- [![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]-->
<!-- [![LinkedIn][linkedin-shield]][linkedin-url]  -->

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/BerndCzech/newinvest">
    <img src="docs/logo.jpg" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">NewInvest-README</h3>

  <p align="center">
    An awesome webside to optimize your portfolio! Visit [stablefolio.eu](https://stablefolio.eu)
    <br />
    <a href="https://gitlab.com/BerndCzech/newinvest"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/BerndCzech/newinvest">View Demo</a>
    ·
    <a href="https://gitlab.com/BerndCzech/newinvest/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/BerndCzech/newinvest/-/issues">Request Feature</a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

<figure style="display: block;
  margin-left: auto;
  margin-right: auto; width:400px;"  >
  <img src="docs/app.png" alt="my alt text" />
  <figcaption>Example Screen Shot of the app.</figcaption>
</figure>

I only came across one page/project that pursues a
similar [idea](https://www.portfoliovisualizer.com/optimize-portfolio#analysisResults). When saving earnings most people
have a guts feeling which assets to invest in. This is not necessarily the most profitable stock it can be effected by
emotions, inheritance or ecological commitment.

The idea of [NewInvest][base-url] is not (only) to be a portfolio optimizer in a Markowitz fashion (e.g. reduce this
position by 12% and raise two other by 6%). The idea is rather to propose another, new investment. This proposal depends
on the current portfolio and optimization patterns.

The app should provide:

- Few alternatives (in new investments, optimization criteria) to choose from
- High Level UI for everyday users
- Provide involving graphics amplifying improvements by the new investment

### Built With

This section should list any major frameworks that you built your project using. Leave any add-ons/plugins for the
acknowledgements section. Here are a few examples.

- [GO/Golang](https://golang.org)
- [Svelte](https://svelte.dev/)
- [TypeScript](https://www.typescriptlang.org/)
- [PostgreSQL](https://www.postgresql.org/)

<!-- GETTING STARTED -->

## Getting Started

<!-- This is an example of how you may give instructions on setting up your project locally. To get a local copy up and running follow these simple example steps. -->

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

- [npm](https://nodejs.org/en/download/)

  ```sh
  npm install npm@latest -g
  ```

- [golang](https://golang.org/doc/install)

### Usage

1. Start data source and sync tool
   ```sh
   docker compose --f deployments/docker-compose.yaml up -d  
   ```
2. a) dev

    ```shell
    go run . &
    cd frontend
    npm run dev
    ```

2. b) Prod build and start the app
   ```sh
   go generate ./...
   go run .
   ```

<!-- USAGE EXAMPLES -->


<!-- _For more examples, please refer to the [Documentation](https://example.com)_ -->

<!-- ROADMAP -->

## Roadmap

See the [open issues](https://gitlab.com/BerndCzech/newinvest/-/issues) for a list of proposed features (and known
issues).

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any
contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE` for more information.

<!-- CONTACT -->

## Contact

Bernd Czech - [@your_twitter](https://twitter.com/your_username) - therealberndczech@gmail.com
Project Link: [base-url]

<!-- ACKNOWLEDGEMENTS -->

## Acknowledgements

- [Img Shields](https://shields.io)
- [Choose an Open Source License](https://choosealicense.com)
- [Animate.css](https://daneden.github.io/animate.css)
- [Loaders.css](https://connoratherton.com/loaders)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[base-url]: https://gitlab.com/BerndCzech/newinvest

[contributors-shield]: https://img.shields.io/badge/CONTRIBUTORS-1-brightgreen?style=for-the-badge

[contributors-url]: https://gitlab.com/BerndCzech/newinvest/-/graphs/main

[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge

[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members

[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge

[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers

[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge

[issues-url]: https://github.com/othneildrew/Best-README-Template/issues

[license-shield]: https://img.shields.io/badge/LICENSE-MIT-blue?style=for-the-badge

[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt

[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555

[linkedin-url]: https://linkedin.com/in/othneildrew

[product-screenshot]: docs/screenshot.png
