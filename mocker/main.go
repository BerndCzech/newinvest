// Package main TODO: should be release as binary
package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

var port = flag.Int("port", 4242, "port to listen for th browser")

func main() {
	flag.Parse()
	dirs, err := os.ReadDir(".")
	if err != nil {
		log.Fatal(err)
	}
	for _, dir := range dirs {
		if !dir.IsDir() {
			continue
		}
		b, err := os.ReadFile(filepath.Join(dir.Name(), "data.json"))
		if err != nil {
			log.Fatal(err)
		}

		http.HandleFunc(fmt.Sprintf("/%s", dir.Name()), jsonHandler(b))
	}
	log.Printf("Starting server on port: %d", *port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), nil))

}

func jsonHandler(json []byte) http.HandlerFunc {

	return func(w http.ResponseWriter, req *http.Request) {
		// heavy computations
		time.Sleep(500 * time.Millisecond)

		w.Header().Set("Content-Type", "text/html; charset=ascii")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type,access-control-allow-origin, access-control-allow-headers")

		//bod, _ := ioutil.ReadAll(req.Body)
		//defer req.Body.Close()
		//fmt.Print(bod)
		if _, err := w.Write(json); err != nil {
			log.Fatal(err)
		}
	}
}
