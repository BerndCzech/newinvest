package main

import (
	"context"
	"flag"
	"fmt"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/log"
	"gitlab.com/BerndCzech/postgresvantage/pkg/client"
	stdLog "log"
	"os"
)

// TODO: add .env, envvar reader or similar000
type envConfig struct {
	Addr string
	DSN  string
}

func main() {

	httpAddr := flag.String("addr", ":4000", "HTTP network address")
	dsn := flag.String("dsn", "postgresql://postgres:seecret@localhost:5433/assetDB", "Postgres data source name")
	flag.Parse()

	e := envConfig{
		Addr: *httpAddr,
		DSN:  *dsn,
	}

	const level = "debug"

	l, err := leveledLogger(level)
	if err != nil {
		fmt.Printf("%+v", err)
		os.Exit(1)
	}

	ctx, cancel := gracefulTerm(l)
	defer cancel()

	if err := run(ctx, l, e); err != nil {
		l.Fatalf("%+v", err)
	}
}

func run(ctx context.Context, logger log.Logger, e envConfig) error {

	// init repo
	pool, err := openDB(ctx, e.DSN)
	if err != nil {
		return err
	}
	defer pool.Close()

	const archPrefix = "data-archive"
	l := stdLog.New(os.Stdout, archPrefix, stdLog.Ltime)
	archive, errChan := client.NewArchive(ctx, l, pool)
	go func() {
		for err := range errChan {
			logger.Errorf("%s: %+v", archPrefix, err)
		}
	}()

	// Start Request Handler
	server, err := pkg.NewEnv(logger, archive, e.Addr, pkg.WithFrontend())
	if err != nil {
		return err
	}
	if err := server.Start(ctx); err != nil {
		logger.Fatalf("%+v", err)
	}
	return nil
}
