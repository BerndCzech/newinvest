package main

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/log"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func leveledLogger(level string) (log.Logger, error) {

	conf := zap.NewProductionConfig()

	l, err := zapcore.ParseLevel(level)
	if err != nil {
		return nil, errors.WithMessage(err, "could not parse level")
	}
	// set options
	conf.Level.SetLevel(l)
	conf.Encoding = "console"

	logger, err := conf.Build()
	if err != nil {
		return nil, errors.WithMessage(err, "could not parse config")
	}
	defer logger.Sync() // flushes buffer, if any

	return logger.Sugar(), nil
}

// The openDB() function wraps sql.Open() and returns a sql.DB connection pool
// for a given DSN.
func openDB(ctx context.Context, dsn string) (*pgxpool.Pool, error) {

	toCTX, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	pool, err := pgxpool.Connect(toCTX, dsn)
	if err != nil {
		return nil, err
	}

	if err := pool.Ping(toCTX); err != nil {
		return nil, err
	}

	return pool, nil
}

func gracefulTerm(l log.Logger) (context.Context, context.CancelFunc) {

	ctx, cancel := context.WithCancel(context.Background())

	// Buffer set to one in case no receiver is listening
	// Leaking go routine is prevented that way
	// If it cant send it's value it'll block
	sigOSEnd := make(chan os.Signal, 1)

	go func() {

		l.Info("Graceful Shutdown Active")
		signal.Notify(sigOSEnd, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
		// Block until a signal is received.
		l.Info("Received signal to close: %s - canceling context", <-sigOSEnd)
		cancel()
	}()

	return ctx, cancel
}
