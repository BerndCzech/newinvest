package summary

import (
	"context"
	ac "gitlab.com/BerndCzech/NewInvest/v2/pkg/antiCorruption"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/finmath"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/log"
	"math"
	"time"
)

type Service struct {
	comPerf ComputePerformance
}

func NewService(l log.Logger, repo TimeSeriesRepo) (Service, error) {
	m, err := finmath.NewMath(repo, l)
	if err != nil {
		return Service{}, err
	}
	return Service{m.PerfMeasures}, nil
}

type TimeSeriesRepo interface {
	Assets(ctx context.Context) ([]ac.PricedAsset, error)
	AssetsByID(ctx context.Context, ids ...ac.AssetID) ([]ac.PricedAsset, error)
	TimeSeriesAssetsByID(ctx context.Context, from time.Time, ids ...ac.AssetID) ([]ac.TimeSeriesAsset, error)
}

type ComputePerformance func(ctx context.Context, position finmath.Positions) (finmath.PerformanceMeasures, error)

func (s Service) Summarize(ctx context.Context, beforeAndAfter TwoPortfolios) ([]Summary, error) {
	pf1, err := s.comPerf(ctx, positionsFromPortfolio(beforeAndAfter.Portfolio))
	if err != nil {
		return nil, err
	}
	pf2, err := s.comPerf(ctx, positionsFromPortfolio(beforeAndAfter.Proposal))
	if err != nil {
		return nil, err
	}

	sry := make([]Summary, 0)
	if !math.IsNaN(pf1.VaR95) && !math.IsNaN(pf2.VaR95) {
		sry = append(sry, Summary{Measure: "Var95 (Abs)", From: pf1.VaR95, To: pf2.VaR95})
	}
	if !math.IsNaN(pf1.ProbSharpeRatio) && !math.IsNaN(pf2.ProbSharpeRatio) {
		sry = append(sry, Summary{Measure: "ProbSharpeRatioGTZero", From: pf1.ProbSharpeRatio, To: pf2.ProbSharpeRatio})
	}
	if !math.IsNaN(pf1.MaxDD) && !math.IsNaN(pf2.MaxDD) {
		sry = append(sry, Summary{Measure: "MaxDD (%)", From: pf1.MaxDD, To: pf2.MaxDD})
	}
	if !math.IsNaN(pf1.RateOfReturn) && !math.IsNaN(pf2.RateOfReturn) {
		sry = append(sry, Summary{Measure: "RateOfReturn", From: pf1.RateOfReturn, To: pf2.RateOfReturn})
	}
	if !math.IsNaN(pf1.SharpeRatio) && !math.IsNaN(pf2.SharpeRatio) {
		sry = append(sry, Summary{Measure: "SharpeRatio", From: pf1.SharpeRatio, To: pf2.SharpeRatio})
	}
	if !math.IsNaN(pf1.StandardDeviation) && !math.IsNaN(pf2.StandardDeviation) {
		sry = append(sry, Summary{Measure: "StandardDeviation", From: pf1.StandardDeviation, To: pf2.StandardDeviation, Inverse: true})
	}

	return sry, nil
}

func positionsFromPortfolio(portfolio Portfolio) finmath.Positions {

	positions := make(finmath.Positions, len(portfolio))
	for _, pos := range portfolio {
		positions[pos.AssetID] = pos.Number
	}
	return positions
}
