package summary

import (
	"encoding/json"
	"math"
)

type Summary struct {
	Measure string  `json:"measure"`
	From    float64 `json:"from"`
	To      float64 `json:"to"`
	Inverse bool    `json:"inverse,omitempty"`
}

// TwoPortfolios expects that element 1 is before and element 2 is after
type TwoPortfolios struct {
	Proposal  Portfolio `json:"proposal"`
	Portfolio Portfolio `json:"portfolio"`
}

type (
	Portfolio []Asset
	Asset     struct {
		AssetID int    `json:"id"`
		Name    string `json:"name"`
		// number of shares hold
		Number int     `json:"number"`
		Price  float64 `json:"price"`
		// Volume = price * number
		Volume float64 `json:"volume"`
	}
)

func (a *Asset) MarshalJSON() ([]byte, error) {

	return json.Marshal(&struct {
		AssetID int    `json:"id"`
		Name    string `json:"name"`
		// number of shares hold
		Number int     `json:"number"`
		Price  float64 `json:"price"`
		// Volume = price * number
		Volume float64 `json:"volume"`
	}{
		AssetID: a.AssetID,
		Name:    a.Name,
		Number:  a.Number,
		Price:   math.Round(a.Price*100) / 100,
		Volume:  math.Round(float64(a.Number)*a.Price*100) / 100,
	})
}
