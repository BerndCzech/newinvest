// Package antiCorruption wraps all **input** parameters for the business domains
// However **output** types should be defined in the particular packages/domains to fit their client requested structure
// I.e. you should not see MarshalJSON methods here
package antiCorruption

import (
	"context"
	"encoding/json"
	"gitlab.com/BerndCzech/postgresvantage/pkg/client"
	"math"
	"time"
)

// Database structs
type (
	AssetID = client.AssetID
	// AssetPositions values show how many of this asset are currently in the portfolio
	AssetPositions  map[AssetID]int
	TimeSeries      = client.TimeSeries
	TimeSeriesAsset = client.TimeSeriesAsset
)

type OwnArchive interface {
	Assets(ctx context.Context) ([]PricedAsset, error)
	AssetsByID(ctx context.Context, ids ...AssetID) ([]PricedAsset, error)
	AssetsBySearch(ctx context.Context, search string) ([]PricedAsset, error)
	TimeSeriesAssetsByID(ctx context.Context, from time.Time, ids ...AssetID) ([]TimeSeriesAsset, error)
}

type ForeignArchive interface {
	Assets(ctx context.Context) ([]client.PricedAsset, error)
	AssetsByID(ctx context.Context, ids ...client.AssetID) ([]client.PricedAsset, error)
	AssetsBySearch(ctx context.Context, search string) ([]client.PricedAsset, error)
	TimeSeriesAssetsByID(ctx context.Context, from time.Time, ids ...client.AssetID) ([]client.TimeSeriesAsset, error)
}

func NewUncorruptedRepo(archive ForeignArchive) OwnArchive {
	return ownArchive{archive}
}

type ownArchive struct {
	f ForeignArchive
}

func (o ownArchive) Assets(ctx context.Context) ([]PricedAsset, error) {
	pas, err := o.f.Assets(ctx)
	if err != nil {
		return nil, err
	}
	newPas := make([]PricedAsset, 0, len(pas))
	for _, pa := range pas {
		newPas = append(newPas, PricedAsset(pa))
	}
	return newPas, nil
}

func (o ownArchive) AssetsByID(ctx context.Context, ids ...AssetID) ([]PricedAsset, error) {

	pas, err := o.f.AssetsByID(ctx, ids...)
	if err != nil {
		return nil, err
	}
	newPas := make([]PricedAsset, 0, len(pas))
	for _, pa := range pas {
		newPas = append(newPas, PricedAsset(pa))
	}
	return newPas, nil
}

func (o ownArchive) AssetsBySearch(ctx context.Context, search string) ([]PricedAsset, error) {
	pas, err := o.f.AssetsBySearch(ctx, search)
	if err != nil {
		return nil, err
	}
	newPas := make([]PricedAsset, 0, len(pas))
	for _, pa := range pas {
		newPas = append(newPas, PricedAsset(pa))
	}
	return newPas, nil
}

func (o ownArchive) TimeSeriesAssetsByID(ctx context.Context, from time.Time, ids ...AssetID) ([]TimeSeriesAsset, error) {
	TSAs, err := o.f.TimeSeriesAssetsByID(ctx, from, ids...)
	if err != nil {
		return nil, err
	}
	return TSAs, nil
}

type PricedAsset client.PricedAsset

func (pa *PricedAsset) MarshalJSON() ([]byte, error) {

	return json.Marshal(&struct {
		ID    int     `json:"id"`
		Name  string  `json:"name"`
		Price float64 `json:"price"`
	}{
		ID:    pa.ID,
		Name:  pa.Name,
		Price: math.Round(pa.Price*100) / 100,
	})
}
