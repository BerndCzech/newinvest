package track

import (
	"encoding/json"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/finmath"
	"math"
)

type (
	Portfolio []Asset
	Asset     struct {
		AssetID int    `json:"id"`
		Name    string `json:"name"`
		// number of shares hold
		Number int     `json:"number"`
		Price  float64 `json:"price"`
		// Volume = price * number
		Volume float64 `json:"volume"`
	}
)

func (pf Portfolio) toPositions() finmath.Positions {

	pos := make(finmath.Positions, len(pf))
	for _, asset := range pf {
		pos[asset.AssetID] = asset.Number
	}
	return pos
}

func (a *Asset) MarshalJSON() ([]byte, error) {

	return json.Marshal(&struct {
		AssetID int    `json:"id"`
		Name    string `json:"name"`
		// number of shares hold
		Number int     `json:"number"`
		Price  float64 `json:"price"`
		// Volume = price * number
		Volume float64 `json:"volume"`
	}{
		AssetID: a.AssetID,
		Name:    a.Name,
		Number:  a.Number,
		Price:   math.Round(a.Price*100) / 100,
		Volume:  math.Round(float64(a.Number)*a.Price*100) / 100,
	})
}
