package track

import (
	"context"
	"github.com/pkg/errors"
	ac "gitlab.com/BerndCzech/NewInvest/v2/pkg/antiCorruption"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/finmath"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/log"
	"gitlab.com/BerndCzech/postgresvantage/pkg/client"
	"math"
	"sort"
	"time"
)

type Service struct {
	makePF func(ctx context.Context, pos finmath.Positions) (finmath.Portfolio, error)
}

func NewService(l log.Logger, repo TimeSeriesRepo) (Service, error) {
	m, err := finmath.NewMath(repo, l)
	if err != nil {
		return Service{}, err
	}
	return Service{m.MakePortfolio}, nil
}

type TimeSeriesRepo interface {
	Assets(ctx context.Context) ([]ac.PricedAsset, error)
	AssetsByID(ctx context.Context, ids ...ac.AssetID) ([]ac.PricedAsset, error)
	TimeSeriesAssetsByID(ctx context.Context, from time.Time, ids ...ac.AssetID) ([]ac.TimeSeriesAsset, error)
}

func (s Service) Records(ctx context.Context, before, after Portfolio) (TwoRecords, error) {

	pfBefore, err := s.makePF(ctx, before.toPositions())
	if err != nil {
		return TwoRecords{}, err
	}
	pfAfter, err := s.makePF(ctx, after.toPositions())
	if err != nil {
		return TwoRecords{}, err
	}
	bef := pfBefore.TrackRecord
	aft := pfAfter.TrackRecord
	client.SyncDates(bef, aft)

	const layout = "2006-01-02" // time.RFC3339
	dates := make([]string, 0, len(bef))
	for t := range bef {
		dates = append(dates, t.Format(layout))
	}
	sort.Strings(dates)

	tR := TwoRecords{
		Time:   dates,
		Before: make([]float64, 0, len(dates)),
		After:  make([]float64, 0, len(dates)),
	}

	bef0 := bef[mustParse(dates[0], layout)] / 100
	aft0 := aft[mustParse(dates[0], layout)] / 100

	for _, date := range dates {
		// normalize both to 100
		tR.Before = append(tR.Before, math.Round(bef[mustParse(date, layout)]/bef0))
		tR.After = append(tR.After, math.Round(aft[mustParse(date, layout)]/aft0))
	}

	//tR := TwoRecords{
	//	Time:   []string{"10-10-2000", "08-10-2000", "20-10-2000", "01-10-2000"},
	//	Before: []float64{0.5, 0.4, 1, 2},
	//	After:  []float64{2, 2, 1.9, 1.8},
	//}
	return tR, nil
}

func mustParse(s string, layout string) time.Time {
	t, err := time.Parse(layout, s)
	if err != nil {
		panic(errors.WithMessage(err, "problem converting time-series date"))
	}
	return t
}

type Positions map[int]int

type TwoRecords struct {
	Time   []string  `json:"time"`
	Before []float64 `json:"before"`
	After  []float64 `json:"after"`
}
