package pkg

import (
	"context"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/antiCorruption"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/log"
	"gitlab.com/BerndCzech/postgresvantage/pkg/client"
	"time"
)

type EnvMode = string

const (
	DEV  EnvMode = "DEV"
	PROD EnvMode = "PROD"
)

func NewEnv(logger log.Logger, a Archive, Addr string, opts ...Option) (Env, error) {

	e := Env{
		arch:        antiCorruption.NewUncorruptedRepo(a),
		logger:      logger,
		addr:        Addr,
		mode:        DEV,
		frontendDir: "",
	}

	for _, opt := range opts {
		if err := opt(&e); err != nil {
			return Env{}, err
		}
	}

	return e, nil
}

type Option func(*Env) error

func WithFrontend() Option {
	return func(env *Env) error {
		const buildDir = "build"
		env.frontendDir = buildDir
		return nil
	}
}

func WithProdMode() Option {
	return func(env *Env) error {
		env.mode = PROD
		return nil
	}
}

type Env struct {
	arch        antiCorruption.OwnArchive
	logger      log.Logger
	addr        string
	mode        EnvMode
	frontendDir string
}

func (e *Env) Repo() antiCorruption.OwnArchive {
	return e.arch
}

type Archive interface {
	Assets(ctx context.Context) ([]client.PricedAsset, error)
	AssetsByID(ctx context.Context, ids ...client.AssetID) ([]client.PricedAsset, error)
	AssetsBySearch(ctx context.Context, search string) ([]client.PricedAsset, error)
	TimeSeriesAssetsByID(ctx context.Context, from time.Time, ids ...client.AssetID) ([]client.TimeSeriesAsset, error)
}
