package web

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/assetSearch"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/log"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/propose"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/summary"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/track"
	"net/http"
	"runtime/debug"
	"time"
)

// Routes registers all handlers calling it more than once will reset
func Routes(log log.Logger, p propose.Service, a assetSearch.Service, s summary.Service, pl track.Service, frontend http.Handler) http.Handler {

	r := chi.NewRouter()
	// A good base middleware stack
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	// Set a timeout value on the request context (ctx), that will signal
	// through ctx.Done() that the request has timed out and further
	// processing should be stopped.
	r.With(middleware.Timeout(5*time.Second)).Get("/search", searchAsset(log, a))
	r.With(middleware.Timeout(30*time.Second)).Post("/propose", proposePortfolio(log, p))
	r.With(middleware.Timeout(50*time.Second)).Post("/summary", summarizePortfolio(log, s))
	r.With(middleware.Timeout(50*time.Second)).Post("/plot", plotPortfolios(log, pl))
	if frontend != nil {
		r.Mount("/", frontend)
	}
	return r
}

func searchAsset(l log.Logger, s assetSearch.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// lock in to business command
		queryString := r.URL.Query().Get("queryString")
		// business logic
		pf, err := s.Search(r.Context(), queryString)
		if err != nil {
			serverError(w, l, err)
			return
		}
		// reply to sender
		js, err := json.Marshal(pf)
		if err != nil {
			serverError(w, l, err)
			return
		}
		if _, err := w.Write(js); err != nil {
			serverError(w, l, err)
		}
		return
	}

}
func proposePortfolio(l log.Logger, s propose.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		var pos propose.Positions
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&pos); err != nil {
			serverError(w, l, err)
			return
		}
		// business logic
		pf, err := s.Suggest(r.Context(), pos)
		if err != nil {
			serverError(w, l, err)
			return
		}
		// reply to sender
		js, err := json.Marshal(pf)
		if err != nil {
			serverError(w, l, err)
			return
		}
		if _, err := w.Write(js); err != nil {
			serverError(w, l, err)
		}
		return
	}

}
func summarizePortfolio(l log.Logger, s summary.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// business logic
		var two summary.TwoPortfolios
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&two); err != nil {
			serverError(w, l, err)
			return
		}
		summ, err := s.Summarize(r.Context(), two)
		if err != nil {
			serverError(w, l, err)
			return
		}
		// reply to sender
		js, err := json.Marshal(summ)
		if err != nil {
			serverError(w, l, err)
			return
		}
		if _, err := w.Write(js); err != nil {
			serverError(w, l, err)
		}
		return
	}
}

func plotPortfolios(l log.Logger, s track.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// business logic
		//var two summary.TwoPortfolios
		//decoder := json.NewDecoder(r.Body)
		//if err := decoder.Decode(&two); err != nil {
		//	serverError(w, l, err)
		//	return
		//}
		two := struct {
			Before track.Portfolio `json:"before"`
			After  track.Portfolio `json:"after"`
		}{}
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&two); err != nil {
			serverError(w, l, err)
			return
		}
		record, err := s.Records(r.Context(), two.Before, two.After)
		if err != nil {
			serverError(w, l, err)
			return
		}
		// reply to sender
		js, err := json.Marshal(record)
		if err != nil {
			serverError(w, l, err)
			return
		}
		if _, err := w.Write(js); err != nil {
			serverError(w, l, err)
		}
	}
}

// The serverError helper writes an error message and stack trace to the errorLog,
// then sends a generic 500 Internal Server Error response to the user.
func serverError(w http.ResponseWriter, l log.Logger, err error) {
	trace := fmt.Sprintf("%s\n%s", err.Error(), debug.Stack())
	l.Error(trace)
	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

// The clientError helper sends a specific status code and corresponding description
// to the user. We'll use this later in the book to send responses like 400 "Bad
// Request" when there's a problem with the request that the user sent.
func clientError(w http.ResponseWriter, status int) {
	http.Error(w, http.StatusText(status), status)
}
