package propose

import (
	"encoding/json"
	ac "gitlab.com/BerndCzech/NewInvest/v2/pkg/antiCorruption"
	"math"
)

type Positions ac.AssetPositions

func (p *Positions) UnmarshalJSON(bytes []byte) error {

	var pf Portfolio
	if err := json.Unmarshal(bytes, &pf); err != nil {
		return err
	}

	pNew := make(Positions, len(pf))
	for _, asset := range pf {
		pNew[asset.AssetID] = asset.Number
	}

	*p = pNew

	return nil
}

type (
	Portfolio []Asset
	Asset     struct {
		AssetID int    `json:"id"`
		Name    string `json:"name"`
		// number of shares hold
		Number int     `json:"number"`
		Price  float64 `json:"price"`
		// Volume = price * number
		Volume float64 `json:"volume"`
	}
)

func (a *Asset) MarshalJSON() ([]byte, error) {

	return json.Marshal(&struct {
		AssetID int    `json:"id"`
		Name    string `json:"name"`
		// number of shares hold
		Number int     `json:"number"`
		Price  float64 `json:"price"`
		// Volume = price * number
		Volume float64 `json:"volume"`
	}{
		AssetID: a.AssetID,
		Name:    a.Name,
		Number:  a.Number,
		Price:   math.Round(a.Price*100) / 100,
		Volume:  math.Round(float64(a.Number)*a.Price*100) / 100,
	})
}
