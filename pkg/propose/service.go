package propose

import (
	"context"
	ac "gitlab.com/BerndCzech/NewInvest/v2/pkg/antiCorruption"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/finmath"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/log"

	"time"

	"github.com/pkg/errors"
)

type Service struct {
	assetRepo func(ctx context.Context, ids ...ac.AssetID) ([]ac.PricedAsset, error)
	math      *finmath.Math
	log       log.Logger
}

type TimeSeriesRepo interface {
	Assets(ctx context.Context) ([]ac.PricedAsset, error)
	AssetsByID(ctx context.Context, ids ...ac.AssetID) ([]ac.PricedAsset, error)
	TimeSeriesAssetsByID(ctx context.Context, from time.Time, ids ...ac.AssetID) ([]ac.TimeSeriesAsset, error)
}

// NewService is mandatory to be build from the constructor as it caches some data at startup
func NewService(l log.Logger, repo TimeSeriesRepo) (Service, error) {
	if repo == nil {
		return Service{}, errors.New("Time series repo can not be nil")
	}

	m, err := finmath.NewMath(repo, l)
	if err != nil {
		return Service{}, err
	}
	o := Service{
		assetRepo: repo.AssetsByID,
		math:      m,
		log:       l,
	}

	return o, nil

}

func (o Service) Suggest(ctx context.Context, pos Positions) (Portfolio, error) {

	acPos := make(ac.AssetPositions, len(pos))
	for id, i := range pos {
		acPos[id] = i
	}
	sug, err := o.math.SuggestPosition(ctx, acPos)
	if err != nil {
		return nil, err
	}

	pf := make([]Asset, 0, len(sug.Positions))
	for id, amount := range sug.Positions {
		a, err := o.assetRepo(ctx, id)
		if err != nil {
			return nil, err
		}
		if len(a) != 1 {
			return nil, errors.Errorf("could not find a asset under id: %d", id)
		}
		pf = append(pf, Asset{
			AssetID: id,
			Name:    a[0].Name,
			Number:  amount,
			Price:   a[0].Price,
		},
		)
	}

	return pf, nil
}
