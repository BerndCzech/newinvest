package assetSearch

import (
	"encoding/json"
	"gitlab.com/BerndCzech/postgresvantage/pkg/client"
	"math"
)

type PricedAsset client.PricedAsset

func (p *PricedAsset) MarshalJSON() ([]byte, error) {

	return json.Marshal(&struct {
		AssetID int    `json:"id"`
		Name    string `json:"name"`
		// number of shares hold
		Price float64 `json:"price"`
	}{
		AssetID: p.ID,
		Name:    p.Name,
		Price:   math.Round(p.Price*100) / 100,
	})
}
