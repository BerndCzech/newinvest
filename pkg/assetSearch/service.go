package assetSearch

import (
	"context"
	"github.com/pkg/errors"
	ac "gitlab.com/BerndCzech/NewInvest/v2/pkg/antiCorruption"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/log"
)

type SearchFunc func(context.Context, string) ([]ac.PricedAsset, error)

// NewService is mandatory to be build from the constructor as it caches some data at startup
func NewService(l log.Logger, sf SearchFunc) (Service, error) {
	if sf == nil {
		return Service{}, errors.New("Time series repo can not be nil")
	}

	o := Service{
		search: sf,
		log:    l,
	}

	return o, nil

}

type Service struct {
	search SearchFunc
	log    log.Logger
}

func (s *Service) Search(ctx context.Context, query string) ([]PricedAsset, error) {

	pas, err := s.search(ctx, query)
	if err != nil {
		return nil, err
	}
	res := make([]PricedAsset, 0, len(pas))
	for _, pa := range pas {
		res = append(res, PricedAsset(pa))
	}
	return res, err
}
