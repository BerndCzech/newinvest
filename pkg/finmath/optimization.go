package finmath

import (
	"context"
	"math/rand"
	"time"
)

func (m *Math) optimizePortfolio(ctx context.Context, pf Portfolio) (Portfolio, error) {

	initialPositions := pf.Positions
	avgVolume := pf.CurrentValue / float64(len(initialPositions))

	//res := make(chan result)

	assets := m.allAssets
	//go func(chan result) {
	//	defer close(res)
	rdmOrder := make([]int, 0, len(assets))
	for i := 0; i < len(assets); i++ {
		rdmOrder = append(rdmOrder, i)
	}
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(rdmOrder), func(i, j int) { rdmOrder[i], rdmOrder[j] = rdmOrder[j], rdmOrder[i] })

	winnerpf := pf
	l := 0
	for i, chosen := range rdmOrder {
		v := assets[chosen]
		m.log.Debugf("Computing %d of %d: rdm item %d", i+1, len(assets), chosen)

		select {
		case <-ctx.Done():
			// need to come back
			return Portfolio{}, ctx.Err()

		default:

		}

		posAmount := int(avgVolume / v.Price)
		posList := make(Positions, len(initialPositions)+1)
		for id, amount := range initialPositions {
			posList[id] = amount
		}

		posList[v.ID] = posAmount
		// this takes most of the time in that function
		curPF, err := m.MakePortfolio(ctx, posList)
		if err != nil {
			m.log.Infof("Skipping Error: %v\n", err)
			continue
		}
		m.log.Debugf("Portfolio %d: %s \ncontains: %v ", i+1, curPF, curPF.Positions)
		//		res <- result{pf, err}
		//	}
		//
		//}(res)
		//
		//winnerpf := pf
		//i := 0
		//
		//m.log.Infof("Initially contains: %v\n", winnerpf.Positions)
		//
		//for pfRes := range res {

		//curPF, err := pfRes.pf, pfRes.err
		if curPF.Performance.MaxDD > winnerpf.Performance.MaxDD &&
			curPF.Performance.RateOfReturn > winnerpf.Performance.RateOfReturn &&
			curPF.Performance.SharpeRatio > winnerpf.Performance.SharpeRatio &&
			// BUG! TODO: Fix this I don´t know why it is sometimes nil
			// use conditional debugging
			curPF.Positions != nil {
			l++
			winnerpf = curPF
		}

		// TODO: remove break
		if l > 2 {
			break
		}
	}

	m.log.Infof("Winner contains: %v\n", winnerpf.Positions)

	//	m.optimizePortfolio(ctx, winnerpf)

	return winnerpf, nil

}
