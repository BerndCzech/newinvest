package finmath

import (
	"github.com/pkg/errors"
	"math"
	"sort"

	"gonum.org/v1/gonum/stat"
	"gonum.org/v1/gonum/stat/distuv"
)

// returns computes the returns
func returns(x []float64) ([]float64, error) {

	if len(x) == 0 {
		return []float64{}, errors.New("got empty list of prices")
	}

	res := make([]float64, len(x)-1)
	for i := range res {
		res[i] = (x[i+1] - x[i]) / x[i]
	}

	return res, nil

}
func logReturns(x []float64) ([]float64, error) {

	if len(x) == 0 {
		return []float64{}, errors.New("got empty list of prices")
	}

	res := make([]float64, len(x)-1)
	for i := range res {
		res[i] = math.Log(x[i+1] / x[i])
	}

	return res, nil

}

// computeRelation
// If len(weights) must be equal to len(shareids), if nil
// then it will be assumed {1,...,1}
func tsAnnualReturnsVarianceVaR(ts TimeSeries) (rr, sd, VaR95, psr0, maxDD float64, err error) {

	dates := sortedDates(ts)
	prices := make([]float64, len(ts))

	for i, v := range dates {
		prices[i] = ts[v]
	}

	// TODO: !!! continue here!!!

	dailyreturn, err := returns(prices)
	if err != nil {
		return math.NaN(), math.NaN(), math.NaN(), math.NaN(), math.NaN(), err
	}

	//const longterm = 300
	// eg. 365 / 30 ~ 12/1
	daysPassed := dates[len(dates)-1].Sub(dates[0]).Hours() / 24
	annualQuotiend := daysPassed / 365

	// compute
	VaR95, err = getVaR(dailyreturn, .95)
	if err != nil {
		return math.NaN(), math.NaN(), math.NaN(), math.NaN(), math.NaN(), err
	}

	// compute annualized standard deviation
	sd = math.Sqrt(stat.Variance(dailyreturn, nil) * annualQuotiend)

	// compute annualized return
	e := ts[dates[len(dates)-1]]
	s := ts[dates[0]]
	rr = math.Pow(e/s, (1/annualQuotiend)) - 1

	// PSR0 prob of SharpeRatio beeing greater than 0
	psr0 = probSharpeRatio(dailyreturn, 10, rr/sd)

	// Compute Maximum Drawdown (negative)
	maxDD = maxDrawDown(prices)

	return rr, sd, VaR95, psr0, maxDD, nil
}

func getVaR(x []float64, confidence float64) (float64, error) {
	if confidence > 1 || confidence <= 0 {
		return 0, errors.Errorf("confidence must be between 0 and 1, got: %f", confidence)
	}

	sorted := make([]float64, len(x))
	copy(sorted, x)
	sort.Float64s(sorted)

	var nthElement int
	if confidence == 1 {
		nthElement = len(x) - 1
	} else {
		nthElement = int(float64(len(x)) * (1 - confidence))
	}

	return sorted[nthElement], nil

}

// probSharpeRatio prob of SharpeRatio beeing greater than 0
func probSharpeRatio(returns []float64, benchmarkSR float64, sr float64) float64 {

	𝛾3 := stat.Skew(returns, nil)
	𝛾4 := stat.ExKurtosis(returns, nil) + 3

	// numerator
	x := (sr - benchmarkSR) * math.Sqrt(float64(len(returns)-1))
	//denominator
	x = x / math.Sqrt(1-𝛾3*sr+(𝛾4-1)/4*math.Pow(sr, 2))

	stdNormal := distuv.Normal{
		Mu:    0,
		Sigma: 1,
	}

	psr := stdNormal.CDF(x)

	return psr
}

// maxDrawDown expects returns to be sorted from oldest to latest Price
// It is realtive to the Maximum and negative. E.g. 100 -> 80 = -0.2 = -20%
func maxDrawDown(prices []float64) float64 {

	var max, min, maxDD float64

	for _, p := range prices {
		if p > max {
			max = p
			min = p

		}
		if p < min {
			min = p
			if (min-max)/max < maxDD {
				maxDD = (min - max) / max
			}
		}

	}

	return maxDD
}
