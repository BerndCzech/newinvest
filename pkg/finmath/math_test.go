package finmath

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gonum.org/v1/gonum/stat"
)

func BenchmarkSum(b *testing.B) {

	// Sums more than everything (all shares 20 years)
	var rSet = make([]float64, 5000*200*20)
	//var rSet32 = make([]float32, 5000*200*20)

	for i := range rSet {
		rSet[i] = rand.Float64() * 100
		//rSet32[i] = rand.Float32() * 100
	}

	b.Run("Normal Returns",
		func(b *testing.B) {
			returns(rSet)
		})
	// log returns are 3 times slower
	b.Run("Log Rerun Method",
		func(b *testing.B) {
			logReturns(rSet)
		})

	b.Run("gonum Mean",
		func(b *testing.B) {
			stat.Mean(rSet, nil)
		})

}

func Test_sortedDates(t *testing.T) {
	type args struct {
		series TimeSeries
	}
	tests := []struct {
		name string
		args args
		want []time.Time
	}{
		{name: "test sorting",
			args: args{TimeSeries{
				time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC): 7,
				time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC): 7,
				time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC): 7,
			}}, want: []time.Time{
				time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC),
				time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
				time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
			}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := sortedDates(tt.args.series)
			assert.Equal(t, got, tt.want)
		})
	}
}

func Test_getVaR(t *testing.T) {
	type args struct {
		x          []float64
		confidence float64
	}
	tests := []struct {
		name    string
		args    args
		want    float64
		wantErr bool
	}{
		{name: "Odd slice length - CI 95%", args: struct {
			x          []float64
			confidence float64
		}{x: []float64{-1, 2, .95, .5, 20}, confidence: .95}, want: -1, wantErr: false},
		{name: "Even slice length - CI 80%", args: struct {
			x          []float64
			confidence float64
			// 6 * 0.8 = 4.8 so on 80% in the days the 2nd worst will not be fallen short of.
		}{x: []float64{-1, 2, .95, .5, 18, 20}, confidence: .80}, want: 0.5, wantErr: false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getVaR(tt.args.x, tt.args.confidence)
			if (err != nil) != tt.wantErr {
				return
			}
			assert.Equalf(t, tt.want, got, "getVaR(%v, %v)", tt.args.x, tt.args.confidence)
		})
	}
}

func Test_maxDrawDown(t *testing.T) {
	type args struct {
		prices []float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "2 values - no drawdown",
			args: struct{ prices []float64 }{prices: []float64{1, 2}},
			want: 0,
		},
		{
			name: "10 values - 20% drawdown",
			args: struct{ prices []float64 }{
				prices: []float64{10, 12, 11, 17.4, 20, 19, 18, 17, 16 /* MaxDD */, 42}},
			want: -.2,
		},
		{
			name: "10 values - 30% drawdown",
			args: struct{ prices []float64 }{
				prices: []float64{10, 12, 8.4 /* MaxDD */, 17.4, 20, 19, 18, 17, 16, 42}},
			want: -.3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, maxDrawDown(tt.args.prices), "maxDrawDown(%v)", tt.args.prices)
		})
	}
}

func Test_returns(t *testing.T) {
	type args struct {
		x []float64
	}
	tests := []struct {
		name    string
		args    args
		want    []float64
		wantErr assert.ErrorAssertionFunc
	}{
		{name: "4 vals",
			args: struct{ x []float64 }{x: []float64{10, 11, 9.9, 11.385}},
			want: []float64{.1, -.1, .15},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := returns(tt.args.x)
			if !tt.wantErr(t, err, fmt.Sprintf("returns(%v)", tt.args.x)) {
				return
			}
			assert.Equalf(t, tt.want, got, "returns(%v)", tt.args.x)
		})
	}
}
