package finmath

import (
	"context"
	"fmt"
	ac "gitlab.com/BerndCzech/NewInvest/v2/pkg/antiCorruption"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/log"
	"time"

	"github.com/pkg/errors"
	"gonum.org/v1/gonum/floats/scalar"
)

type (
	AssetID         = ac.AssetID
	PricedAsset     = ac.PricedAsset
	TimeSeries      = ac.TimeSeries
	TimeSeriesAsset = ac.TimeSeriesAsset
)

// Math type creates a custom model which wraps the sql.DB connection pool.
type Math struct {
	log                  log.Logger
	allAssets            []PricedAsset
	assetsByID           func(ctx context.Context, ids ...ac.AssetID) ([]ac.PricedAsset, error)
	timeSeriesAssetsByID func(ctx context.Context, from time.Time, ids ...ac.AssetID) ([]ac.TimeSeriesAsset, error)
}

func NewMath(archive TimeSeriesRepo, log log.Logger) (*Math, error) {

	if archive == nil {
		return nil, errors.New("missing TimeSeriesRepo")
	}
	as, err := archive.Assets(context.Background())
	if err != nil {
		return nil, errors.WithStack(err)
	}
	log.Info("Initially cached all assets")
	m := &Math{
		assetsByID:           archive.AssetsByID,
		allAssets:            as,
		timeSeriesAssetsByID: archive.TimeSeriesAssetsByID,
		log:                  log}

	go func() {
		for {
			// refresh available assets every 30 minutes
			time.Sleep(30 * time.Minute)

			as, err := archive.Assets(context.Background())
			if err != nil {
				// better luck next time
				continue
			}
			m.allAssets = as
		}
	}()
	return m, nil
}

type TimeSeriesRepo interface {
	Assets(ctx context.Context) ([]ac.PricedAsset, error)
	AssetsByID(ctx context.Context, ids ...ac.AssetID) ([]ac.PricedAsset, error)
	TimeSeriesAssetsByID(ctx context.Context, from time.Time, ids ...ac.AssetID) ([]ac.TimeSeriesAsset, error)
}

// Positions holds all assets and
// their number of elements (*not* monetary value)
type Positions = ac.AssetPositions

// timeSeries does not apply the amount of the position
// This is done for efficiency as tsAdd loops elementwise anyways
// However tsAdd(Timeseries,amount) can be applied fo a singe asset as well
func (m *Math) timeSeries(ctx context.Context, shareid AssetID) (TimeSeries, error) {
	ts, err := m.timeSeriesAssetsByID(ctx, time.Now().AddDate(-4, 0, 0), shareid)
	if err != nil {
		return nil, errors.Wrapf(err, "could not find time series for shareID: %d", shareid)
	}
	if len(ts) != 1 {
		return nil, errors.Errorf("could not find time series for shareID: %d", shareid)
	}
	return ts[0].TimeSeries, nil
}

// Portfolio contains all investment information
type Portfolio struct {
	Positions    Positions
	TrackRecord  TimeSeries
	Performance  PerformanceMeasures
	CurrentValue float64
}

// PerformanceMeasures contains all long- and
// short Term risk and return measures
type PerformanceMeasures struct {
	RateOfReturn      float64
	StandardDeviation float64
	SharpeRatio       float64
	VaR95             float64
	ProbSharpeRatio   float64
	MaxDD             float64
}

func (pf Portfolio) String() string {

	str := fmt.Sprintf("Portfolio has a Performance of %v and a volume of %f",
		pf.Performance, pf.CurrentValue)

	return str
}

// PerfMeasures adds financial data to a list of positions
func (m *Math) PerfMeasures(ctx context.Context, pos Positions) (PerformanceMeasures, error) {

	pms, err := m.MakePortfolio(ctx, pos)
	if err != nil {
		return PerformanceMeasures{}, err
	}

	return pms.Performance, nil
}

// MakePortfolio adds financial data to a list of positions
func (m *Math) MakePortfolio(ctx context.Context, pos Positions) (Portfolio, error) {

	var (
		tsList     []TimeSeries
		amounts    []float64
		totalValue float64
	)

	for id, amount := range pos {
		if amount < 1 {
			continue
		}
		ts, err := m.timeSeries(ctx, id)
		if err != nil {
			return Portfolio{}, err
		}
		// TODO(optimize): all ts at one query - already prepared
		tsList = append(tsList, ts)
		amounts = append(amounts, float64(amount))
		aPriced, err := m.assetByID(ctx, id)
		if err != nil {
			return Portfolio{}, err
		}
		totalValue += aPriced.Price * float64(amount)
	}

	ts, err := add(amounts, tsList...)
	// TODO: remove hard coded 20
	//  (a) this is only to prevent a too short TrackRecord
	//  (b) some PerformanceMeasures need some longer time
	if err != nil || len(ts) < 20 {
		return Portfolio{}, err
	}

	pm, err := makePerformanceMeasures(ts, totalValue)
	if err != nil {
		return Portfolio{}, err
	}

	return Portfolio{pos, ts, pm, totalValue}, nil
}

func makePerformanceMeasures(ts TimeSeries, pfvolume float64) (PerformanceMeasures, error) {
	rr, sd, VaR95, psr0, maxDD, err := tsAnnualReturnsVarianceVaR(ts)
	if err != nil {
		return PerformanceMeasures{}, err
	}
	return PerformanceMeasures{RateOfReturn: scalar.Round(rr, 4), StandardDeviation: scalar.Round(sd, 4),
		SharpeRatio: scalar.Round(rr/sd, 4), VaR95: scalar.Round(VaR95*pfvolume, 4),
		MaxDD: scalar.Round(maxDD, 4), ProbSharpeRatio: scalar.Round(psr0, 4),
	}, nil
}

func (m *Math) assetByID(ctx context.Context, id AssetID) (PricedAsset, error) {
	//now := time.Now()
	v, err := m.assetsByID(ctx, id)
	if err != nil || len(v) < 1 {
		return PricedAsset{}, errors.WithMessagef(err, "could not find AssetID: %d", id)
	}
	//m.log.Debugf("%s elapsed for searching asset", time.Since(now))
	return v[0], nil
}

// SuggestPosition advises to buy several new Assets
// Todo: Does a pointer make more sense here?
func (m *Math) SuggestPosition(ctx context.Context, originalPF Positions) (Portfolio, error) {

	if originalPF == nil {
		return Portfolio{}, nil
	}

	pf, err := m.MakePortfolio(ctx, originalPF)
	if err != nil {
		return Portfolio{}, err
	}

	m.log.Info("Preparing a suggestion")

	winnerPF, err := m.optimizePortfolio(ctx, pf)
	if err != nil {
		return Portfolio{}, err
	}

	return winnerPF, nil
}
