package finmath

import (
	"sort"
	"time"

	"gitlab.com/BerndCzech/postgresvantage/pkg/client"
)

func add(weights []float64, tslist ...TimeSeries) (TimeSeries, error) {
	return client.Add(weights, tslist...)
}

func sortedDates(series TimeSeries) []time.Time {
	dates := make([]time.Time, len(series))
	i := 0
	for date := range series {
		dates[i] = date
		i++
	}

	sort.Slice(dates, func(i, j int) bool {
		return dates[i].Before(dates[j])
	})

	return dates
}
