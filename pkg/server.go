package pkg

import (
	"context"
	"gitlab.com/BerndCzech/NewInvest/v2/frontend"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/assetSearch"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/propose"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/summary"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/track"
	"gitlab.com/BerndCzech/NewInvest/v2/pkg/web"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

// Start starts the Application Server
// Port is
func (e *Env) Start(ctx context.Context) error {

	e.logger.Info("--------------------------")
	e.logger.Infof("Started Server at %s", e.addr)
	defer e.logger.Info("---- Server Stopped --------")

	repo := e.Repo()
	prop, err := propose.NewService(e.logger, repo)
	if err != nil {
		return err
	}
	search, err := assetSearch.NewService(e.logger, repo.AssetsBySearch)
	if err != nil {
		return err
	}
	summ, err := summary.NewService(e.logger, repo)
	if err != nil {
		return err
	}
	plot, err := track.NewService(e.logger, repo)
	if err != nil {
		return err
	}

	var fe http.Handler
	if e.frontendDir != "" {
		fe = frontend.SvelteHandler(e.frontendDir)
	}

	routing := web.Routes(e.logger, prop, search, summ, plot, fe)

	if e.mode == DEV {
		routing = freeCORS(routing)
	}
	s := &http.Server{
		Addr:    e.addr,
		Handler: routing,

		IdleTimeout:  60 * time.Second,
		ReadTimeout:  60 * time.Second,
		WriteTimeout: 60 * time.Second,
	}
	errChan := make(chan error, 1)
	go func() {
		err := s.ListenAndServe()
		errChan <- err
	}()

	select {
	case err := <-errChan:
		return errors.Wrap(err, "could not start server")

	case <-ctx.Done():
		e.logger.Errorf("terminated with context error: %v", ctx.Err())
	}

	// Graceful Shutdown
	e.logger.Infof("shutting down the server within the next 3 seconds")
	tc, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	return errors.Wrap(s.Shutdown(tc), "")

}

func freeCORS(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//Allow CORS here By * or specific origin
		w.Header().Add("Access-Control-Allow-Origin", "*")
		next.ServeHTTP(w, r)
	})
}
